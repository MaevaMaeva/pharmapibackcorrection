//At the top of your file :
$url = parse_url(getenv("CLEARDB_DATABASE_URL"));
$host = $url["host"] ?? null;
$username = $url["user"] ?? null;
$password = $url["pass"] ?? null;
$database = substr($url["path"], 1);
// Just after the return [ :
'default' => env('DB_CONNECTION', 'your_heroku_mysql_connection'),
//In the 'connections' array :
'your_heroku_mysql_connection' => array(
            'driver' => 'mysql',
            'host' => $host,
            'database' => $database,
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ),